;; -*- no-byte-compile: t; -*-
;;; .doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:host github :repo "username/repo"))
;; (package! builtin-package :disable t)
(package! dired-narrow)
(package! exunit)
(package! org-projectile)
(package! persistent-scratch)
(package! shrink-whitespace)
;;(package! which-key-posframe)
